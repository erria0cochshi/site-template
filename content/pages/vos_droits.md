---
title: "Vos droits"
date: "{{ .Date }}"
draft: false
---


Droit à la formation
====================

L'union s'est dotée d'un organisme de formation syndical, le CEFI. Au titre du CFESS, tous les salarié.es ont droit à une formation (12 jours par an), payée par <NOM_DE_L_ENTREPRISE>.

Le **congé de formation économique, sociale, environnementale et syndicale** (CFESES) est un droit à la formation : il donne droit à 12 jours de formation par an, salaire maintenu par . Solidaires prend en charge les frais de participation pour ses adhérent·es.

- [Le CFESES (service-public.fr)](https://www.service-public.fr/particuliers/vosdroits/F2320)
- [Se former avec le CEFI (Solidaires)](https://solidaires.org/se-former/)
- [Le CFESES - Art. L.2145-5 (_Code du travail_)](https://code.travail.gouv.fr/code-du-travail/l2145-5)


Droit des salarié·es
====================

- [Fiches « connaître ces droits » (Solidaires)](https://solidaires.org/connaitre-ses-droits/fiche-droits/)
- …

Droit de Grève
===============

- [Le droit de grève (Solidaires)](https://solidaires.org/connaitre-ses-droits/fiche-droits/fiche-n-7-le-droit-de-greve/)
- [La grève, c'est cool (-;](https://greve.cool/)
- [Est-ce que c'est la grève ?](https://nitter.fdn.fr/cestlagreve)
- …
