---
title: "Notre Programme"
date: "{{ .Date }}"
draft: false
---


## Sommaire

1. [Nos priorités](#priorites)
   - …
1. [Représentation des salarié·es](#representation)
   - 📚 [Le CSE](#cse)
   - 🧑‍⚖️ [Défense des salarié·es](#defense)
   - 🗣️ [Représentant·es de proximité](#rp)
   - …
1. [Conditions de travail](#conditions)
   - 💶 [Salaires](#salaires)
   - 🧑‍💼 [Management](#management)
   - 🏠 [Télétravail](#teletravail)
   - 🛠️ [Outillage](#outillage)
   - 🚲 [Mobilités](#velo)
   - …
1. [Protection des salarié·es](#protect)
   - 🌈 [Lutte contre les discriminations](#discrimination)
   - ⛑️ [Sécurité au travail](#secu)
   - 🩺 [Bien-être et santé au travail](#bien-etre)
   - …
1. …
   - …

## 1- Nos priorités {#priorites}

<INTRO>

1. …
1. …
1. …


## 2- Représentation des salarié·es {#representation}


### 📚 Le CSE {#cse}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


## 🧑‍⚖️ Défense des salarié·es {#defense}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


## 🗣️ Représentant·es de proximité {#rp}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


## 3- Conditions de travail {#conditions}


### 💶 Salaires {#salaires}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


### 🧑‍💼 Management {#management}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


### 🏠 Télétravail {#teletravail}

<INTRO>


#### Les constats

* …
* …


#### Nos Propositions

* …
* …


### 🛠️ Outillage {#outillage}

> L'employeur est tenu d'assurer aux salariés les moyens d'assurer leurs missions. Or, certains outils <ENTREPRISE> nous handicapent plus qu'ils ne nous aident.


#### Les constats

* …
* …

#### Nos Propositions

* …
* …


### 🚲 Mobilités {#velo}


<INTRO>

#### Les constats

* …
* …

#### Nos Propositions

* …
* …


## 4- Protection des salarié·es {#protect}


### 🌈 Lutte contre les discriminations {#discrimination}

#### Les constats

* …
* …

#### Nos Propositions

* …
* …


### ⛑️ Sécurité au travail {#secu}

> Ce point aborde les problématiques de sécurité au travail, particulièrement de l'organisation des secours en cas d'incident (malaise, incendie…) demandant une intervention urgente et immédiate.


<INTRO>

#### Les constats

* …
* …

#### Nos Propositions

* …
* …


### 🩺 Bien-être et santé au travail {#bien-etre}

> Ce point aborde les questions de bien-être et de santé au travail, entre autres s'agissant de la prévention et de la prise en charge des risques psychosociaux ou des TMS et autres maladies professionnelles.


<INTRO>

#### Les constats

* …
* …

#### Nos Propositions

* …
* …
